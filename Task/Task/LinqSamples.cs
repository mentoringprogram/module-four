﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.Linq;
using SampleSupport;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
	[Title("LINQ Module")]
	[Prefix("Linq")]
	public class LinqSamples : SampleHarness
	{

		private DataSource dataSource = new DataSource();

        [Category("Restriction Operators")]
        [Title("Where - Task 1")]
        [Description("This linq gets a list of all customers whose total turnover exceeds some value of X = 6000")]

        public void Linq001()
        {
            decimal x = 5000;
            var customersList = dataSource.Customers
            .Where(c => c.Orders.Sum(o => o.Total) > x);

            foreach (var a in customersList)
            {
                ObjectDumper.Write(a);
            }
        }

        [Category("Join Operators")]
        [Title("Join - Task 2")]
        [Description("This linq makes a list of suppliers located in the same country and the same city for each client")]

        public void Linq002()
        {
            var suppliers = dataSource.Customers.Join(dataSource.Suppliers,
                            cust => cust.Country,
                            supp => supp.Country,
                            (cust, supp) => new
                            {
                                Customer = cust,
                                Supplier = supp
                            }
                            ).Select(p1 => new { p1.Customer.CustomerID, p1.Supplier.SupplierName });

            foreach (var s in suppliers)
            {
                ObjectDumper.Write(s);
            }
        }

        [Category("Restriction Operators")]
        [Title("Where - Task 3")]
        [Description("This linq finds all customers who have orders that exceed the sum of X = 6000")]
        public void Linq3()
        {
            decimal x = 10;

            var orders = dataSource.Customers
                .Where(c => c.Orders.Any(o => o.Total > x))
                .Select(c => c.CompanyName);

            //OR
            //var orders = from c in dataSource.Customers
            //         from o in c.Orders
            //         where o.Total > x
            //         select c.CompanyName;

            foreach (var o in orders)
                ObjectDumper.Write(o);
        }

        [Category("Select Operators")]
        [Title("Select - Task 4")]
        [Description("This linq gets a list of customers including the month of the year they became clients (first order month as a required date)")]
        public void Linq4()
        {
            var customers = dataSource.Customers
                .Select(customer => new
                {
                    Name = customer.CompanyName,
                    Date = customer.Orders.Min(order => order.OrderDate)
                });

            foreach (var c in customers)
                ObjectDumper.Write(c);
        }

        [Category("Order Operators")]
        [Title("OrderBy - Task 5")]
        [Description("This is previous task, but the list is geted sorted by year, month, customer turnover (from the maximum to the minimum), and the client's name")]
        public void Linq5()
        {
            var firstDate = dataSource.Customers
                .Select(customer => new
                {
                    MoneyTotal = customer.Orders.Sum(or => or.Total),
                    Name = customer.CompanyName,
                    Date = customer.Orders.Length > 0 ? customer.Orders.Min(date => date.OrderDate) : DateTime.MinValue
                })
                .OrderBy(_ => _.Date.Year)
                .ThenBy(_ => _.Date.Month)
                .ThenBy(_ => _.MoneyTotal)
                .ThenBy(_ => _.Name);

            foreach (var f in firstDate)
                ObjectDumper.Write(f);
        }

        [Category("Restriction Operators")]
        [Title("Where - Task 6")]
        [Description("This linq specifies all customers who have a non-digital postal code, or the region is not filled, or the operator code is not specified in the phone")]
        public void Linq6()
        {
            var clients = dataSource.Customers.Where(cust =>
                !cust.PostalCode.All(char.IsDigit) ||
                string.IsNullOrEmpty(cust.Region) ||
                !cust.Phone.StartsWith("(")
            );

            foreach (var c in clients)
                ObjectDumper.Write(c);
        }

        [Category("Group Operators")]
        [Title("GroupBy - Task 7")]
        [Description("This linq qroups all products by category, inside - by stock, within the last group sort by cost")]
        public void Linq7()
        {
            var groupProducts = dataSource.Products
                .GroupBy(prod => prod.Category, (categ, elem) => new
                {
                    Category = categ,
                    Elements = elem.GroupBy(item => item.UnitsInStock, (units, prods) => new
                    {
                        Count = units,
                        Products = prods.OrderBy(cost => cost.UnitPrice).ToList()
                    }).ToList()
                });

            foreach (var c in groupProducts)
            {
                ObjectDumper.Write(c.Category);

                foreach (var d in c.Elements)
                {
                    ObjectDumper.Write(d.Count);

                    foreach (var e in d.Products)
                        ObjectDumper.Write(e);
                }
            }
        }

        [Category("Group Operators, Restriction Operators")]
        [Title("GroupBy, Where - Task 8")]
        [Description("This linq groups the goods into groups Cheap, Average Price, Expensive")]
        public void Linq8()
        {
            var cheap = dataSource.Products.Where(product => product.UnitPrice < 10);
            var avg = dataSource.Products.GroupBy(product => product.UnitPrice >= 10 && product.UnitPrice < 40);
            var high = dataSource.Products.GroupBy(product => product.UnitPrice >= 40);

            ObjectDumper.Write("----Cheap----");
            foreach (var c in cheap)
                ObjectDumper.Write(c);

            ObjectDumper.Write("----Average----");
            foreach (var a in avg)
                ObjectDumper.Write(a);

            ObjectDumper.Write("----High Price----");
            foreach (var h in high)
                ObjectDumper.Write(h);
        }

        [Category("Group Operators")]
        [Title("GroupBy - Task 9")]
        [Description("This linq calculates the average profitability of each city and the average intensity")]
        public void Linq9()
        {
            var avg = dataSource.Customers.GroupBy(cust => cust.City, (city, customer) => new
            {
                City = city,
                AvgProfit = customer.Average(c => c.Orders.Sum(o => o.Total)),
                Intensivity = customer.Average(c => c.Orders.Length)
            });

            foreach (var a in avg)
                ObjectDumper.Write(a);
        }


        [Category("Group Operators")]
        [Title("GroupBy - Task 10")]
        [Description("This linq makes the average annual activity statistics of clients by month, statistics by year, by year and by month")]
        public void Linq10()
        {
            var orders = dataSource.Customers.SelectMany(cust => cust.Orders).ToArray();

            var byMonth = orders.GroupBy(order => order.OrderDate.Month, (monthNumber, ordersCount) => new
            {
                Month = monthNumber,
                Statistics = ordersCount.Count()
            }).OrderBy(stat => stat.Month);

            ObjectDumper.Write("By Month");

            foreach (var a in byMonth)
                ObjectDumper.Write(a);

            var byYear = orders.GroupBy(order => order.OrderDate.Year, (yearNumber, ordersCount) => new
            {
                Year = yearNumber,
                Statistics = ordersCount.Count()
            }).OrderBy(stat => stat.Year);

            ObjectDumper.Write("By Year");

            foreach (var a in byYear)
                ObjectDumper.Write(a);

            var byYearAndMonth = orders.GroupBy(order => new { order.OrderDate.Year, order.OrderDate.Month },
                (yearMonth, ordersCount) => new
                {
                    YearMonth = yearMonth,
                    Statistics = ordersCount.Count()
                });

            ObjectDumper.Write("By Year and Month");

            foreach (var a in byYearAndMonth)
                ObjectDumper.Write($"{a.YearMonth}, {a.Statistics}");
        }

    }
}
